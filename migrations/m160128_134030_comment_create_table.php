<?php

use yii\db\Schema;
use yii\db\Migration;

class m160128_134030_comment_create_table extends Migration {

    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }


        $this->createTable('{{%comment}}', [
            'id'          => Schema::TYPE_PK,
            'tree'        => Schema::TYPE_INTEGER,
            'lft'         => Schema::TYPE_INTEGER  . ' NOT NULL',
            'rgt'         => Schema::TYPE_INTEGER  . ' NOT NULL',
            'depth'       => Schema::TYPE_INTEGER  . ' NOT NULL',
            'body'        => Schema::TYPE_STRING   . ' NOT NULL',
            'user_id'     => Schema::TYPE_STRING,
            'article_id'  => Schema::TYPE_INTEGER  . ' NOT NULL',
            'created_at'  => Schema::TYPE_INTEGER  . ' NOT NULL',
            'updated_at'  => Schema::TYPE_INTEGER  . ' NOT NULL',
            'status'      => Schema::TYPE_STRING   . ' NOT NULL'

                ], $tableOptions);

        $this->createIndex('idx_comment_status', '{{%comment}}', 'status');
        
//        $this->addForeignKey('fk_comment_user_id',    '{{%comment}}', 'user_id',    '{{%user}}',    'id');
//        $this->addForeignKey('fk_comment_article_id', '{{%comment}}', 'article_id', '{{%article}}', 'id');
    }

    public function safeDown() {
        $this->dropTable('{{%comment}}');
    }

}
