<?php

namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;

/**
 * Description of CommentWidget
 *
 * @author yiimar
 */
class CommentWidget extends Widget
{
    /**
     * Величина отступа от родительского блока, в пикселях
     */
    private $leftMargin = 20;

    private $comment;
    private $margin;

    public function init($comment, $margin = null)
    {
        $this->comment    = $comment;
        $this->leftMargin = $margin
                ? (int) $margin
                : $this->leftMargin;

        $this->margin  = $this->comment->depth * $this->leftMargin;
    }

    public function run()
    {
        $comment_str = <<<COMMENT
<a class="comment">
    <div
        class="comment-item well"
        style="margin-left: {$this->margin}px;"
    >
        {$this->comment->body}
    </div>
</a>
COMMENT;

        echo $comment_str;
    }
}
