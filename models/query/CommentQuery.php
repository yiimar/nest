<?php

namespace app\models\query;

use app\models\Comment;
use creocoder\nestedsets\NestedSetsQueryBehavior;

/**
 * This is the ActiveQuery class for [[\app\models\Comment]].
 *
 * @see \app\models\Comment
 */
class CommentQuery extends \yii\db\ActiveQuery
{
    // ключ для массива атрибутов
    const CCOMMENT_SEARCH_SESSION_KEY = 'CommentSearchFormData';

    /**
     * Инициализируем текущие параметры фильтра из сессии
     */
    public function init()
    {
        $session = Yii::$app->session;

        if ($this->load(Yii::$app->request->queryParams) && $this->validate()) :
            $session->set(self::CCOMMENT_SEARCH_SESSION_KEY, $this->getAttributes());
        else :
            if (is_array($data = $session->get(self::CCOMMENT_SEARCH_SESSION_KEY))) : 
                $this->setAttributes($data);
            endif;
        endif;
    }

    public function behaviors()
    {
        return [
            NestedSetsQueryBehavior::className(),
        ];
    }

    public function active()
    {
        $this->andWhere('[[status]]=' . Comment::STATUS_ACTIVE);
        return $this;
    }

    /**
     * @inheritdoc
     * @return \app\models\Comment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\Comment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}