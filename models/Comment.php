<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use creocoder\nestedsets\NestedSetsBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%comment}}".
 *
 * @property integer $id
 * @property integer $tree
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property string $body
 * @property string $user_id
 * @property integer $article_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $status
 */
class Comment extends \yii\db\ActiveRecord
{
    const STATUS_MODERATED = 0;
    const STATUS_ACTIVE     = 1;

    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value'              => new Expression('NOW()'),
            ],
            'tree' => [
                'class'           => NestedSetsBehavior::className(),
                 'treeAttribute'  => 'tree',
                 'leftAttribute'  => 'lft',
                 'rightAttribute' => 'rgt',
                 'depthAttribute' => 'depth',
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%comment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tree', 'lft', 'rgt', 'depth', 'article_id', 'created_at', 'updated_at'], 'integer'],
            [['lft', 'rgt', 'depth', 'body', 'article_id', 'created_at', 'updated_at', 'status'], 'required'],
            [['body', 'user_id', 'status'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'tree'       => 'Tree',
            'lft'        => 'Lft',
            'rgt'        => 'Rgt',
            'depth'      => 'Depth',
            'body'       => 'Body',
            'user_id'    => 'User ID',
            'article_id' => 'Article ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status'     => 'Status',
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\query\CommentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\CommentQuery(get_called_class());
    }
}
