<?php

namespace app\models\engines;

use yii\base\Component;
use app\models\Comment;

/**
 * Description of NodeMaker
 *
 * @author yiimar
 */
class NodeMaker extends Component
{
    /**
     * To make a root node
     */
    public function addNewRoot($article_id, $user_id, $body)
    {
        $comment = new Comment([
            'body' => $body,
            'user_id' => $user_id,
            'article_id' => $article_id,
        ]);
        return $comment->makeRoot();
    }

    /**
     * Prepending a node as the first child of another node
     */
    public function addFirstChild($comment, $user_id, $body)
    {
        $sub = new Comment([
            'body' => $body,
            'user_id' => $user_id,
            'article_id' => $comment->article_id,       // название статьи берем у родителя
        ]);
        return $sub->prependTo($comment);               // добавляемся к родителю
    }
    
    /**
     * Getting the root nodes
     */
    public function rootNodes($article_id)
    {
        $roots = Comment::find(['article_id' => $article_id,])
                ->roots()
                ->all();
        return $roots;
    }

    /**
     * Getting children of a node
     */
    public function getNodeChildren($comment, $level = null)
    {
        $children = $comments
                ->children($level)
                ->all();
        return $children;
    }
}
