<?php

return [
    'class'    => 'yii\db\Connection',
    'dsn'      => 'mysql:host=localhost;dbname=nest',
    'username' => 'nest',
    'password' => 'nest',
    'charset'  => 'utf8',
];
